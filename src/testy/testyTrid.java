import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tridy.Akce;
import tridy.Organizator;
import tridy.Zakaznik;

import static org.junit.Assert.*;


public class testyTrid {

        // Před testem
        @Before
        public void setUp() {

        }
        // Po testu
        @After
        public void tearDown() {
        }

        /*
        Test vytvoření zákazníka a vypsání jeho celého jména a kontrola pohlaví
         */
        @Test
        public void testZakazníka()
        {
            Zakaznik zak = new Zakaznik();
            zak.setJmeno("Petr");
            zak.setPrijmeni("Novák");
            zak.setPohlavi("Muž");
            zak.celeJmeno();
            assertEquals("Petr Novák",zak.getCeleJmeno());
            assertEquals("Muž",zak.getPohlavi());
        }

        /*
        Test vytvoření organizátora a vypsání jeho celého jména
         */
        @Test
        public void testOrganiztora()
        {
            Organizator org = new Organizator();
            org.setJmeno("Petr");
            org.setPrijmeni("Novák");
            org.celeJmeno();
            assertEquals("Petr Novák",org.getCeleJmeno());
        }

        /*
        Test vytvoření akce a vypsání všech ID
         */
        @Test
        public void testAkce()
        {
            Organizator org = new Organizator();
            org.setId(2);
            org.setJmeno("Petr");
            org.setPrijmeni("Novák");
            org.celeJmeno();
            assertEquals("Petr Novák",org.getCeleJmeno());

            Zakaznik zak = new Zakaznik();
            zak.setId(5);
            zak.setJmeno("Petr");
            zak.setPrijmeni("Novák");
            zak.setPohlavi("Muž");
            zak.celeJmeno();
            assertEquals("Petr Novák",zak.getCeleJmeno());
            assertEquals("Muž",zak.getPohlavi());

            Akce akce = new Akce();
            akce.setID(1);
            akce.setID_Zakaznika(5);
            akce.setID_Organizatora(2);

            assertEquals(1,akce.getID());
            assertEquals(2,akce.getID_Organizatora());
            assertEquals(5,akce.getID_Zakaznika());
        }

}
