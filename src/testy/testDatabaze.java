import databaze.Databaze;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tridy.Akce;
import tridy.Organizator;
import tridy.Zakaznik;

import static org.junit.Assert.*;


public class testDatabaze {
    private Databaze databaze;

        // Před testem
        @Before
        public void setUp() {
            databaze = new Databaze();
        }
        // Po testu
        @After
        public void tearDown() {
        }

        /*
        Test upraví již existujícího zákazníka a na konci ho vrátí do původního stavu před testem
        Kvůli testu vznikla nová metoda v zákazníkovi - kopíruj, která ho zkopíruje.
         */
        @Test
        public void testUpravyZakaznika()
        {
            // Nejdříve si vytáhnu zákazníka
            Zakaznik testovaci = databaze.getZakaznik(1);
            // Pak ho uložím do další pomocné promněnné
            Zakaznik puvodni = Zakaznik.kopiruj(testovaci);
            // Změním mu jméno a přijmení
            testovaci.setJmeno("Test");
            testovaci.setPrijmeni("Testovič");
            // Pošlu úpravu do databáze
            databaze.upravZakaznika(testovaci);
            // Zavolám si ho z databáze znovu
            Zakaznik novy = databaze.getZakaznik(1);
            // Kontrola že jsou to dva rozdílní zákazníci - jméno a přijmení nepasuje
            assertFalse(puvodni.getJmeno().equals(novy.getJmeno()));
            assertFalse(puvodni.getPrijmeni().equals(novy.getPrijmeni()));
            // Náprava databáze starým záznamem
            databaze.upravZakaznika(puvodni);
            // Načtení záznamu
            novy = databaze.getZakaznik(1);
            // kontrola, že teď už se jejich jména opět shodují, úprava prošla úspěšně
            assertTrue(puvodni.getJmeno().equals(novy.getJmeno()));
            assertTrue(puvodni.getPrijmeni().equals(novy.getPrijmeni()));
        }

}
