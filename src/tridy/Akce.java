package tridy;

import java.time.LocalDate;

public class Akce {
    private int ID;
    private int ID_Organizatora;
    private int ID_Zakaznika;
    private String jmenoOrganizatora;
    private String jmenoZakaznika;
    private String misto;
    private LocalDate datum;
    private String cas;
    private String nazev;
    private int kapacita;
    private int cena;
    private int dobaTrvani;
    private String typAkce;


    public Akce() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID_Organizatora() {
        return ID_Organizatora;
    }

    public void setID_Organizatora(int ID_Organizatora) {
        this.ID_Organizatora = ID_Organizatora;
    }

    public int getID_Zakaznika() {
        return ID_Zakaznika;
    }

    public void setID_Zakaznika(int ID_Zakaznika) {
        this.ID_Zakaznika = ID_Zakaznika;
    }

    public String getJmenoOrganizatora() {
        return jmenoOrganizatora;
    }

    public void setJmenoOrganizatora(String jmenoOrganizatora) {
        this.jmenoOrganizatora = jmenoOrganizatora;
    }

    public String getJmenoZakaznika() {
        return jmenoZakaznika;
    }

    public void setJmenoZakaznika(String jmenoZakaznika) {
        this.jmenoZakaznika = jmenoZakaznika;
    }

    public String getMisto() {
        return misto;
    }

    public void setMisto(String misto) {
        this.misto = misto;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getKapacita() {
        return kapacita;
    }

    public void setKapacita(int kapacita) {
        this.kapacita = kapacita;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getDobaTrvani() {
        return dobaTrvani;
    }

    public void setDobaTrvani(int dobaTrvani) {
        this.dobaTrvani = dobaTrvani;
    }

    public String getTypAkce() {
        return typAkce;
    }

    public void setTypAkce(String typAkce) {
        this.typAkce = typAkce;
    }
}
