package tridy;

import java.time.LocalDate;

public class Zakaznik extends Osoba{
    private String email;
    private int telefon;
    private String pohlavi;
    private LocalDate datumNarozeni;

    public Zakaznik() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTelefon() {
        return telefon;
    }

    public void setTelefon(int telefon) {
        this.telefon = telefon;
    }

    public String getPohlavi() {
        return pohlavi;
    }

    public void setPohlavi(String pohlavi) {
        this.pohlavi = pohlavi;
    }

    public LocalDate getDatumNarozeni() {
        return datumNarozeni;
    }

    public void setDatumNarozeni(LocalDate datumNarozeni) {
        this.datumNarozeni = datumNarozeni;
    }
    // Testovací metoda
    public static Zakaznik kopiruj( Zakaznik zak ) {
        Zakaznik novy = new Zakaznik();
        novy.setId(zak.getId());
        novy.setJmeno(zak.getJmeno());
        novy.setPrijmeni(zak.getPrijmeni());
        novy.celeJmeno();
        novy.setPohlavi(zak.getPohlavi());
        novy.setTelefon(zak.getTelefon());
        novy.setEmail(zak.email);
        novy.setDatumNarozeni(zak.datumNarozeni);
        return novy;
    }
}
