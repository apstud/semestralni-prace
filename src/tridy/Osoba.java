package tridy;

public class Osoba {
    private int id;
    private String jmeno;
    private String prijmeni;
    private String celeJmeno;

    public Osoba() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public void celeJmeno()
    {
        celeJmeno = this.jmeno + " " + this.prijmeni;
    }

    public String getCeleJmeno() {
        return celeJmeno;
    }
}
