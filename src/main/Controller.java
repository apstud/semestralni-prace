package main;

import databaze.Databaze;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import tridy.Akce;
import tridy.Organizator;
import tridy.Zakaznik;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;


public class Controller implements Initializable {

    // Zmapování grafiky s controllerem
    @FXML
    public Label zakaznik,organizator,misto,cas,kapacita,cena,trvani;
    @FXML
    public AnchorPane prihlasovani,menu,akce,zakaznici,formularZakaznik,formularAkce;
    @FXML
    public TextField fJmeno,fPrijmeni,fEmail,fTelefon,username;
    @FXML
    public PasswordField heslo;
    @FXML
    public DatePicker fDatumNarozeni,fDatumKonani;
    @FXML
    public ChoiceBox<String> fPohlavi,fTypAkce;
    @FXML
    public TextField fMisto,fCas,fNazev,fKapacita,fCena,fTrvani;
    @FXML
    public TableView<Akce> tabulkaAkci;
    @FXML
    public TableColumn<Akce,String> nazevAkce,typAkce;
    @FXML
    public TableColumn<Akce,Integer> IDakce,IDzakAkce,IDorg;
    @FXML
    public TableColumn<Akce, LocalDate> datumAkce;
    @FXML
    public TableView<Zakaznik> tabulkaZakazniku,tabulkaZakFormular;
    @FXML
    public TableColumn<Zakaznik,String> jmeno,email,pohlavi,jmenoZakFormular;
    @FXML
    public TableColumn<Zakaznik,Integer> IDzak,telefon,IDZakFormular;
    @FXML
    public TableColumn<Zakaznik, LocalDate> narozeni;
    @FXML
    public Menu menuOdhlasit;

    // Deklarace pomocných entit pro běh programu
    private Databaze databaze = new Databaze(); // Instance databáze
    private ObservableList<Akce> listAkci = FXCollections.observableArrayList();
    private ObservableList<Zakaznik> listZakazniku = FXCollections.observableArrayList();
    private ObservableList<String> listPohlavi = FXCollections.observableArrayList();
    private ObservableList<String> listTypuAkci = FXCollections.observableArrayList();
    private Organizator prihlaseny; // přihlášený uživatel provadějící změny
    private AnchorPane aktivniOkno;
    private Akce upravovanaAkce;
    private Zakaznik upravovanyZak;
    private boolean uprava = false; // false - nový / true - upravit

    // Metoda pro přihlášení do aplikace
    // Pošle dotaz do databáze na uživatele a kontroluje zda se heslo které se s ním vrátí shoduje se zadaným
    @FXML
    public void prihlasit()
    {
        prihlaseny = databaze.getOrganizator(username.getText());
        if(prihlaseny.getHeslo().equals(heslo.getText()))
        {
            listAkci = databaze.listAkci();
            tabulkaAkci.setItems(listAkci);
            prihlasovani.setVisible(false);
            menu.setVisible(true);
            aktivniOkno = akce;
        }
    }
    // Metoda odhlásí uživatele a uvede aplikaci do výchozího stavu
    @FXML
    public void odhlasit()
    {
        // výchozí stav
        prihlasovani.setVisible(true);
        menu.setVisible(false);
        akce.setVisible(true);
        zakaznici.setVisible(false);
        formularZakaznik.setVisible(false);
        formularAkce.setVisible(false);
        prihlaseny = null;
        smazPolicka();
    }
    // Metoda přejde na panel zákazníků a nahraje je do tabulky
    @FXML
    public void zakaznici()
    {
        smazPolicka();
        listZakazniku = databaze.listZakazniku();
        tabulkaZakazniku.setItems(listZakazniku);
        zmenOkno(zakaznici);
    }
    // Metoda přejde na formulář nového zákazníka
    @FXML
    public void pridejZak()
    {
        smazPolicka();
        zmenOkno(formularZakaznik);
        uprava = false;
    }
    // Metoda přejde na formulář úpravy zákazníka
    // Vyplní současné hodnoty do políček
    @FXML
    public void upravZak()
    {
        if(tabulkaZakazniku.getSelectionModel().getSelectedItem() != null)
        {
            upravovanyZak = tabulkaZakazniku.getSelectionModel().getSelectedItem();
            fJmeno.setText(upravovanyZak.getJmeno());
            fPrijmeni.setText(upravovanyZak.getPrijmeni());
            fEmail.setText(upravovanyZak.getEmail());
            fTelefon.setText(upravovanyZak.getTelefon()+"");
            fPohlavi.setValue(upravovanyZak.getPohlavi());
            fDatumNarozeni.setValue(upravovanyZak.getDatumNarozeni());
            zmenOkno(formularZakaznik);
            uprava = true;
        }
        else
        {
            databaze.Error("Je nutné vybrat zákazníka pro úpravu");
        }
    }
    // Metoda zobrazí panel akcí a nahraje akce z databáze do tabulky
    @FXML
    public void akce()
    {
        smazPolicka();
        listAkci = databaze.listAkci();
        tabulkaAkci.setItems(listAkci);
        zmenOkno(akce);
    }
    // Metoda přejde na formulář akcí a připraví ho k použití
    @FXML
    public void pridejAkci()
    {
        smazPolicka();
        listTypuAkci = databaze.typAkci();
        fTypAkce.setItems(listTypuAkci);
        listZakazniku = databaze.listZakazniku();
        tabulkaZakFormular.setItems(listZakazniku);
        zmenOkno(formularAkce);
        uprava = false;
    }
    // Metoda přejde na formulář akcí a předvyplní políčka vybranou akcí ke změně
    @FXML
    public void upravAkci()
    {
        if(tabulkaAkci.getSelectionModel().getSelectedItem() != null)
        {
            listTypuAkci = databaze.typAkci();
            fTypAkce.setItems(listTypuAkci);
            listZakazniku = databaze.listZakazniku();
            tabulkaZakFormular.setItems(listZakazniku);
            uprava = true;
            upravovanaAkce = tabulkaAkci.getSelectionModel().getSelectedItem();

            fMisto.setText(upravovanaAkce.getMisto());
            fCas.setText(upravovanaAkce.getCas());
            fNazev.setText(upravovanaAkce.getNazev());
            fKapacita.setText(upravovanaAkce.getKapacita()+"");
            fCena.setText(upravovanaAkce.getCena()+"");
            fMisto.setText(upravovanaAkce.getMisto());
            fTrvani.setText(upravovanaAkce.getDobaTrvani()+"");
            fDatumKonani.setValue(upravovanaAkce.getDatum());
            fTypAkce.setValue(upravovanaAkce.getTypAkce());

            // Hledání zákazníka akce v seznamu aby se mohl označit v tabulce
            for(Zakaznik zak : listZakazniku)
            {
                if(zak.getId() == upravovanaAkce.getID_Zakaznika()) {
                    tabulkaZakFormular.getSelectionModel().select(zak);
                    break;
                }
            }
            zmenOkno(formularAkce);
        }
        else
        {
            databaze.Error("Je nutné vybrat akci pro úpravu");
        }
    }
    // Metoda přiřazená všem řádkům tabulky akcí
    // Metoda po kliknutí na řádek akce vypíše detaily akce do pravého informačního panelu
    @FXML
    public void detailAkce()
    {
        Akce vybrana = tabulkaAkci.getSelectionModel().getSelectedItem();
        zakaznik.setText(vybrana.getJmenoZakaznika());
        organizator.setText(vybrana.getJmenoOrganizatora());
        misto.setText(vybrana.getMisto());
        cas.setText(vybrana.getCas());
        kapacita.setText(vybrana.getKapacita()+" míst");
        cena.setText(vybrana.getCena()+" Kč");
        trvani.setText(vybrana.getDobaTrvani()+" minut");
    }
    // Metoda potvrzuje změny zákazníka
    // pokuď je zákazník nový tak se vytvoří
    // pokuď měníme zákazníka tak se změní podle hodnot které jsou v políčkách formuláře
    @FXML
    public void potvrditZakaznika()
    {
        if(uprava)
        {
            upravovanyZak.setJmeno(fJmeno.getText());
            upravovanyZak.setPrijmeni(fPrijmeni.getText());
            upravovanyZak.setDatumNarozeni(fDatumNarozeni.getValue());
            upravovanyZak.setPohlavi(fPohlavi.getValue());
            upravovanyZak.setEmail(fEmail.getText());
            upravovanyZak.setTelefon(Integer.parseInt(fTelefon.getText()));

            if(databaze.upravZakaznika(upravovanyZak))
            {
                zakaznici();
            }
            else
            {
                databaze.Error("Jedno nebo více políček je špatně vyplněno");
            }
        }
        else
        {
            Zakaznik novy = new Zakaznik();
            novy.setJmeno(fJmeno.getText());
            novy.setPrijmeni(fPrijmeni.getText());
            novy.setDatumNarozeni(fDatumNarozeni.getValue());
            novy.setPohlavi(fPohlavi.getValue());
            novy.setEmail(fEmail.getText());
            novy.setTelefon(Integer.parseInt(fTelefon.getText()));

            if(databaze.vytvorZakaznika(novy))
            {
                zakaznici();
            }
            else
            {
                databaze.Error("Jedno nebo více políček je špatně vyplněno");
            }
        }
    }
    // Metoda pro přechod zpět z formuláře zákazníků na obrazovku zákazníků
    @FXML
    public void zpetZakaznik()
    {
        zmenOkno(zakaznici);
        smazPolicka();
    }
    // Metoda která potvrzuje formulář akcí
    // Podle toho zda upravujeme nebo vytváříme se vybere správná metoda do databáze.
    @FXML
    public void potvrditAkci()
    {
        if(tabulkaZakFormular.getSelectionModel().getSelectedItem() != null && fTypAkce.getSelectionModel().getSelectedItem() != null)
        {
            if(uprava)
            {
                upravovanaAkce.setCas(fCas.getText());
                upravovanaAkce.setCena(Integer.parseInt(fCena.getText()));
                upravovanaAkce.setDatum(fDatumKonani.getValue());
                upravovanaAkce.setDobaTrvani(Integer.parseInt(fTrvani.getText()));
                upravovanaAkce.setTypAkce(fTypAkce.getValue());
                upravovanaAkce.setNazev(fNazev.getText());
                upravovanaAkce.setMisto(fMisto.getText());
                upravovanaAkce.setKapacita(Integer.parseInt(fKapacita.getText()));
                upravovanaAkce.setID_Organizatora(prihlaseny.getId());
                upravovanaAkce.setID_Zakaznika(tabulkaZakFormular.getSelectionModel().getSelectedItem().getId());

                if(databaze.upravAkci(upravovanaAkce))
                {
                    akce();
                }
                else
                {
                    databaze.Error("Jedno nebo více políček je špatně vyplněno");
                }
            }
            else
            {
                Akce nova = new Akce();
                nova.setCas(fCas.getText());
                nova.setCena(Integer.parseInt(fCena.getText()));
                nova.setDatum(fDatumKonani.getValue());
                nova.setDobaTrvani(Integer.parseInt(fTrvani.getText()));
                nova.setTypAkce(fTypAkce.getValue());
                nova.setNazev(fNazev.getText());
                nova.setMisto(fMisto.getText());
                nova.setKapacita(Integer.parseInt(fKapacita.getText()));
                nova.setID_Organizatora(prihlaseny.getId());
                nova.setID_Zakaznika(tabulkaZakFormular.getSelectionModel().getSelectedItem().getId());

                if(databaze.vytvorAkci(nova))
                {
                    akce();
                }
                else
                {
                    databaze.Error("Jedno nebo více políček je špatně vyplněno");
                }
            }
        }
        else
        {
            databaze.Error("Je nutné vybrat zákazníka a typ akce");
        }
    }
    // Metoda slouží pro přechod zpět do obrazovky akcí z formuláře akcí
    @FXML
    public void zpetAkce()
    {
        zmenOkno(akce);
        smazPolicka();
    }
    // Metoda sloužící pro změnu obrazovky
    // Obrazovky jsou jednotlivé anchor pany, které jsou viditelné nebo neviditelné
    // Metoda zavolá současné aktivní okno a zneviditelní ho
    // následně nahraje do současného okna nové okno poslané jako argument
    // a toto okno zobrazí
    public void zmenOkno(AnchorPane novy)
    {
        aktivniOkno.setVisible(false);
        aktivniOkno = novy;
        aktivniOkno.setVisible(true);
    }

    // Metoda na mazání všech vyplněných údajů po odhlášení nebo jiné akci
    public void smazPolicka()
    {
        fJmeno.setText("");
        fPrijmeni.setText("");
        fEmail.setText("");
        fTelefon.setText("");
        username.setText("");
        heslo.setText("");
        fMisto.setText("");
        fCas.setText("");
        fNazev.setText("");
        fKapacita.setText("");
        fCena.setText("");
        fTrvani.setText("");
        fDatumNarozeni.setValue(null);
        fDatumKonani.setValue(null);
        fPohlavi.setValue(null);
        fTypAkce.setValue(null);
    }

    // Metoda která se spustí jako první po spuštění controlleru
    // Metoda mapuje tabulky -> spojuje její políčka se třídami
    // Metoda také nastavuje list pohlaví
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        IDakce.setCellValueFactory(new PropertyValueFactory<Akce,Integer>("ID"));
        IDzakAkce.setCellValueFactory(new PropertyValueFactory<Akce,Integer>("ID_Zakaznika"));
        IDorg.setCellValueFactory(new PropertyValueFactory<Akce,Integer>("ID_Organizatora"));
        nazevAkce.setCellValueFactory(new PropertyValueFactory<Akce,String>("Nazev"));
        typAkce.setCellValueFactory(new PropertyValueFactory<Akce,String>("typAkce"));
        datumAkce.setCellValueFactory(new PropertyValueFactory<Akce,LocalDate>("datum"));

        IDzak.setCellValueFactory(new PropertyValueFactory<Zakaznik,Integer>("id"));
        telefon.setCellValueFactory(new PropertyValueFactory<Zakaznik,Integer>("telefon"));
        IDZakFormular.setCellValueFactory(new PropertyValueFactory<Zakaznik,Integer>("id"));
        jmeno.setCellValueFactory(new PropertyValueFactory<Zakaznik,String>("celeJmeno"));
        email.setCellValueFactory(new PropertyValueFactory<Zakaznik,String>("email"));
        pohlavi.setCellValueFactory(new PropertyValueFactory<Zakaznik,String>("pohlavi"));
        jmenoZakFormular.setCellValueFactory(new PropertyValueFactory<Zakaznik,String>("celeJmeno"));
        narozeni.setCellValueFactory(new PropertyValueFactory<Zakaznik,LocalDate>("datumNarozeni"));

        listPohlavi.add("Muž");
        listPohlavi.add("Žena");
        fPohlavi.setItems(listPohlavi);

        // Alternativní button v menu na odhlášení
        /*
        menuOdhlasit.setGraphic(
                ButtonBuilder.create()
                        .text("Odhlásit se")
                        .onAction(new EventHandler<ActionEvent>(){
                            @Override public void handle(ActionEvent t) {
                               odhlasit();
                            } })
                        .build()
        );

         */
    }
}
