package databaze;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import tridy.Akce;
import tridy.Organizator;
import tridy.Zakaznik;

/*
Třída se stará o komunikaci s databází
Jako první je definovaný connection - databáze je připojená na localhost
Dále třída obsahuje metoddy pro práci s databází
 */
public class Databaze {
    private Connection con;
    private Statement st;
    private ResultSet rs;
    public Databaze()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/test?useUnicode=yes&characterEncoding=UTF-8", "root", "");
            st = con.createStatement();
        }
        catch (Exception ex)
        {
            System.out.println("Error: " + ex);
        }
    }
// Metoda vrací všechny zákazníky z databáze
    public ObservableList<Zakaznik> listZakazniku()
    {
        try {
            String query = "SELECT * FROM `Zakaznici`";
            rs = st.executeQuery(query);
            ObservableList<Zakaznik> list = FXCollections.observableArrayList();
            while(rs.next())
            {
                Zakaznik zak = new Zakaznik();
                zak.setId(rs.getInt(1));
                zak.setJmeno(rs.getString(2));
                zak.setPrijmeni(rs.getString(3));
                zak.celeJmeno();
                zak.setEmail(rs.getString(4));
                zak.setTelefon(rs.getInt(5));
                zak.setPohlavi(rs.getString(6));
                Date input = rs.getDate(7);
                LocalDate date = Instant.ofEpochMilli(input.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
                zak.setDatumNarozeni(date);
                list.add(zak);
            }
            return list;
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
        return null;
    }
 // Metoda vrací zákazníka z databáze podle ID zákazníka
    public Zakaznik getZakaznik(int id)
    {
        try {
            String query = "SELECT * FROM `Zakaznici` WHERE id = \"" + id + "\"";
            rs = st.executeQuery(query);
            Zakaznik zak = new Zakaznik();
            while(rs.next())
            {
                zak.setId(rs.getInt(1));
                zak.setJmeno(rs.getString(2));
                zak.setPrijmeni(rs.getString(3));
                zak.celeJmeno();
                zak.setEmail(rs.getString(4));
                zak.setTelefon(rs.getInt(5));
                zak.setPohlavi(rs.getString(6));
                Date input = rs.getDate(7);
                LocalDate date = Instant.ofEpochMilli(input.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
                zak.setDatumNarozeni(date);
            }
            return zak;
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
        return null;
    }
 // Metoda vrací list všech typů akcí
    public ObservableList<String> typAkci()
    {
        try {
            String query = "SELECT * FROM `typAkce`";
            rs = st.executeQuery(query);
            ObservableList<String> list = FXCollections.observableArrayList();
            while(rs.next())
            {
                list.add(rs.getString(2));
            }
            return list;
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
        return null;
    }
// Metoda vrací organizátora podle uživatelského jména.
    public Organizator getOrganizator(String uzivatelskeJmeno)
    {
        try {
            String query = "SELECT * FROM `Organizatori` WHERE uzivatelskeJmeno = \"" + uzivatelskeJmeno + "\"";
            rs = st.executeQuery(query);
            Organizator organizator = new Organizator();
            while(rs.next())
            {
                organizator.setId(rs.getInt(1));
                organizator.setUzivatelskeJmeno(rs.getString(4));
                organizator.setHeslo(rs.getString(5));
                organizator.celeJmeno();
            }
            return organizator;
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
        return null;
    }
    // Metoda vrací list všech akcí z databáze
    // V rámci zrychlení práce už rovnou joinuje související tabulky dohromady
    // Tím pádem je možné rovnou nahrát jména organizátora a zákazníka k akci bez doprovodných SQL dotazů
    public ObservableList<Akce> listAkci()
    {
        try {
            String query = "SELECT Akce.*,organizatori.jmeno,organizatori.prijmeni,zakaznici.jmeno,zakaznici.prijmeni " +
                    "FROM `Akce` " +
                    "LEFT JOIN `Organizatori` ON organizatori.id=akce.ID_Organizatora " +
                    "LEFT JOIN `Zakaznici` ON zakaznici.id=akce.ID_Zakaznika;";
            rs = st.executeQuery(query);
            ObservableList<Akce> list = FXCollections.observableArrayList();
            while(rs.next())
            {
                Akce akce = new Akce();
                akce.setID(rs.getInt(1));
                akce.setID_Organizatora(rs.getInt(2));
                akce.setID_Zakaznika(rs.getInt(3));
                akce.setMisto(rs.getString(4));
                Date input = rs.getDate(5);
                LocalDate date = Instant.ofEpochMilli(input.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
                akce.setDatum(date);
                akce.setCas(rs.getString(6));
                akce.setNazev(rs.getString(7));
                akce.setKapacita(rs.getInt(8));
                akce.setCena(rs.getInt(9));
                akce.setDobaTrvani(rs.getInt(10));
                akce.setTypAkce(rs.getString(11));
                akce.setJmenoOrganizatora(rs.getString(12) +" " + rs.getString(13));
                akce.setJmenoZakaznika(rs.getString(15) +" " + rs.getString(14));
                list.add(akce);
            }
            return list;
        }
        catch(Exception ex)
        {
            System.out.println(ex);
        }
        return null;
    }
// Metoda vytváří nového zákazníka
    public boolean vytvorZakaznika(Zakaznik z)
    {
        boolean vysledek = false;
        try {
            String query = "Insert into `Zakaznici` SET `jmeno`= \"" + z.getJmeno()
                    + "\",`prijmeni`= \"" + z.getPrijmeni()
                    + "\",`email`= \"" + z.getEmail()
                    + "\",`telefon`= \"" + z.getTelefon()
                    + "\",`pohlavi`= \"" + z.getPohlavi()
                    + "\",`datumNarozeni`= \"" + z.getDatumNarozeni() + "\"";
            st.executeUpdate(query);
            vysledek = true;
            Uspech();
        } catch (Exception ex) {
            Error(ex.toString());
        }
        return vysledek;
    }
    // Metoda upravuje zákazníka
    public boolean upravZakaznika(Zakaznik z)
    {
        boolean vysledek = false;
        try {
            String query = "Update `Zakaznici` SET `jmeno`= \"" + z.getJmeno()
                    + "\",`prijmeni`= \"" + z.getPrijmeni()
                    + "\",`email`= \"" + z.getEmail()
                    + "\",`telefon`= \"" + z.getTelefon()
                    + "\",`pohlavi`= \"" + z.getPohlavi()
                    + "\",`datumNarozeni`= \"" + z.getDatumNarozeni()
                    + "\" WHERE id = \"" + z.getId() + "\"";
            st.executeUpdate(query);
            vysledek = true;
            //Uspech();
        } catch (Exception ex) {
            Error(ex.toString());
        }
        return vysledek;
    }
    // Metoda vytváří akci
    public boolean vytvorAkci(Akce a)
    {
        boolean vysledek = false;
        try {
            String query = "Insert into `Akce` SET `ID_Organizatora`= \"" + a.getID_Organizatora()
                    + "\",`ID_Zakaznika`= \"" + a.getID_Zakaznika()
                    + "\",`misto`= \"" + a.getMisto()
                    + "\",`datum`= \"" + a.getDatum()
                    + "\",`cas`= \"" + a.getCas()
                    + "\",`nazev`= \"" + a.getNazev()
                    + "\",`kapacita`= \"" + a.getKapacita()
                    + "\",`cena`= \"" + a.getCena()
                    + "\",`dobaTrvani`= \"" + a.getDobaTrvani()
                    + "\",`typAkce`= \"" + a.getTypAkce() + "\"";
            st.executeUpdate(query);
            vysledek = true;
            Uspech();
        } catch (Exception ex) {
            Error(ex.toString());
        }
        return vysledek;
    }
    // Metoda upravuje akci
    public boolean upravAkci(Akce a)
    {
        boolean vysledek = false;
        try {
            String query = "Update `Akce` SET `ID_Organizatora`= \"" + a.getID_Organizatora()
                    + "\",`ID_Zakaznika`= \"" + a.getID_Zakaznika()
                    + "\",`misto`= \"" + a.getMisto()
                    + "\",`datum`= \"" + a.getDatum()
                    + "\",`cas`= \"" + a.getCas()
                    + "\",`nazev`= \"" + a.getNazev()
                    + "\",`kapacita`= \"" + a.getKapacita()
                    + "\",`cena`= \"" + a.getCena()
                    + "\",`dobaTrvani`= \"" + a.getDobaTrvani()
                    + "\",`typAkce`= \"" + a.getTypAkce()
                    + "\" WHERE ID = \"" + a.getID() + "\"";
            st.executeUpdate(query);
            vysledek = true;
            Uspech();
        } catch (Exception ex) {
            Error(ex.toString());
        }
        return vysledek;
    }



    // Metoda pro výpis chyby
    public void Error(String error)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Chyba!");
        alert.setHeaderText("Jedno nebo více políček je špatně");
        alert.setContentText(error);
        alert.showAndWait();
    }
    // Metoda pro výpis úspěchu
    public void Uspech()
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Oznámení");
        alert.setHeaderText("Úspěch");
        alert.setContentText("Operace proběhla úspěšně");
        alert.showAndWait();
    }
    // Metoda pro výpis speciálního úspěchu
    public void Uspech(String uspech)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Oznámení");
        alert.setHeaderText("Úspěch");
        alert.setContentText(uspech);
        alert.showAndWait();
    }


}
